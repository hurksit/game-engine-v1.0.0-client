package it.hurks.util;

import it.hurks.rendering.core.entity.Camera;
import org.lwjgl.util.vector.Matrix4f;
import org.lwjgl.util.vector.Vector3f;

/**
 * This class represents all math related functionality.
 *
 * @author Hurks IT
 */
public class MathUtil {

    /**
     * Create a transformation matrix from translation, rotation and scale.
     *
     * @param translation the translation
     * @param rotation the rotation
     * @param scale the scale
     * @return the transformation matrix
     */
    public static Matrix4f createTransformationMatrix (Vector3f translation, Vector3f rotation, Vector3f scale) {
        Matrix4f transformationMatrix = new Matrix4f();
        transformationMatrix.setIdentity();
        Matrix4f.translate(translation, transformationMatrix, transformationMatrix);
        Matrix4f.rotate((float)Math.toRadians(rotation.x), new Vector3f(1,0,0), transformationMatrix, transformationMatrix);
        Matrix4f.rotate((float)Math.toRadians(rotation.y), new Vector3f(0,1,0), transformationMatrix, transformationMatrix);
        Matrix4f.rotate((float)Math.toRadians(rotation.z), new Vector3f(0,0,1), transformationMatrix, transformationMatrix);
        Matrix4f.scale(scale, transformationMatrix, transformationMatrix);
        return transformationMatrix;
    }

    /**
     * Create a view matrix from a camera object.
     *
     * @param camera the camera
     * @return the view matrix
     */
    public static Matrix4f createViewMatrix (Camera camera) {
        Matrix4f viewMatrix = new Matrix4f();
        viewMatrix.setIdentity();
        Matrix4f.rotate ((float) Math.toRadians(camera.getPitch()), new Vector3f(1, 0, 0), viewMatrix, viewMatrix);
        Matrix4f.rotate ((float) Math.toRadians(camera.getYaw()), new Vector3f(0, 1, 0), viewMatrix, viewMatrix);
        Vector3f negativeCameraPosition = new Vector3f(-camera.getPosition().x, -camera.getPosition().y, -camera.getPosition().z);
        Matrix4f.translate(negativeCameraPosition, viewMatrix, viewMatrix);
        return viewMatrix;
    }

}
