package it.hurks.util;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

/**
 * This class contains all file processing related functionality.
 *
 * @author Hurks IT
 */
public class FileUtil {

    /**
     * Get the contents of a file on the system.
     * @param fileName the file name
     * @return the contents of the file
     */
    public static StringBuilder getContents (String fileName) {
        StringBuilder content = new StringBuilder();
        try {
            BufferedReader reader = new BufferedReader(new FileReader(fileName));
            String line;

            while ((line = reader.readLine()) != null) {
                content.append(line).append("\n");
            }
            reader.close();
        } catch (IOException e) {
            System.err.println("Could not read file!");
            e.printStackTrace();
        }
        return content;
    }

}
