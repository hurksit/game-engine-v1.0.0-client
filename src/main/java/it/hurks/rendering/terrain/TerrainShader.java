package it.hurks.rendering.terrain;

import it.hurks.rendering.core.Shader;
import it.hurks.rendering.core.constants.VertexArrayIndices;
import it.hurks.rendering.core.entity.light.Light;
import org.lwjgl.util.vector.Matrix4f;
import org.lwjgl.util.vector.Vector3f;

/**
 * This class handles all the shading of terrains.
 *
 * @author Hurks IT
 */
public class TerrainShader extends Shader {

    private static final String VERTEX_SHADER_LOCATION = "src/main/resources/shader/terrain/terrainVertexShader.glsl";

    private static final String FRAGMENT_SHADER_LOCATION = "src/main/resources/shader/terrain/terrainFragmentShader.glsl";

    private int locationTransformationMatrix;

    private int locationProjectionMatrix;

    private int locationViewMatrix;

    private int locationLightPosition;

    private int locationLightColor;

    private int locationShineDamper;

    private int locationReflectivity;

    private int locationFogDensity;

    private int locationFogGradient;

    private int locationSkyColor;

    public TerrainShader() {
        super(VERTEX_SHADER_LOCATION, FRAGMENT_SHADER_LOCATION);
    }

    /**
     * Loads the transformation matrix to GLSL.
     *
     * @param matrix the transformation matrix
     */
    public void loadTransformationMatrix (Matrix4f matrix) {
        super.loadMatrix(locationTransformationMatrix, matrix);
    }

    /**
     * Loads the projection matrix to GLSL.
     *
     * @param matrix the projection matrix
     */
    public void loadProjectionMatrix (Matrix4f matrix) {
        super.loadMatrix(locationProjectionMatrix, matrix);
    }

    /**
     * Loads the view matrix to GLSL.
     *
     * @param matrix the view matrix
     */
    public void loadViewMatrix (Matrix4f matrix) {
        super.loadMatrix(locationViewMatrix, matrix);
    }

    /**
     * Loads a light to GLSL.
     *
     * @param light the light to load.
     */
    public void loadLight (Light light) {
        super.loadVector3(locationLightPosition, light.getPosition());
        super.loadVector3(locationLightColor, light.getColor());
    }

    /**
     * Load the variables for specular lightning to OpenGL.
     *
     * @param shineDamper the distance the camera has to be in order to see any reflected light.
     * @param reflectivity the strength of the lights' reflection.
     */
    public void loadSpecularLightningValues (float shineDamper, float reflectivity) {
        super.loadFloat(locationShineDamper, shineDamper);
        super.loadFloat(locationReflectivity, reflectivity);
    }

    /**
     * Load the fog settings to OpenGL.
     *
     * @param fogDensity the density
     * @param fogGradient the gradient
     * @param skyColor color of the sky
     */
    public void loadFogSettings (float fogDensity, float fogGradient, Vector3f skyColor) {
        super.loadFloat(locationFogDensity, fogDensity);
        super.loadFloat(locationFogGradient, fogGradient);
        super.loadVector3(locationSkyColor, skyColor);
    }

    @Override
    protected void getUniformLocations() {
        locationTransformationMatrix = getUniformLocation("transformationMatrix");
        locationProjectionMatrix = getUniformLocation("projectionMatrix");
        locationViewMatrix = getUniformLocation("viewMatrix");
        locationLightPosition = getUniformLocation("lightPosition");
        locationLightColor = getUniformLocation("lightColor");
        locationShineDamper = getUniformLocation("shineDamper");
        locationReflectivity = getUniformLocation("reflectivity");
        locationFogDensity = getUniformLocation("fogDensity");
        locationFogGradient = getUniformLocation("fogGradient");
        locationSkyColor = getUniformLocation("skyColor");
    }

    @Override
    protected void bindAttributes() {
        super.bindAttribute(VertexArrayIndices.POSITIONS, "position");
        super.bindAttribute(VertexArrayIndices.TEXTURE_COORDINATES, "textureCoordinates");
        super.bindAttribute(VertexArrayIndices.NORMALS, "normals");
    }
}
