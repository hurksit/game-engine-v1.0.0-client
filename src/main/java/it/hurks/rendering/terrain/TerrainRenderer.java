package it.hurks.rendering.terrain;

import it.hurks.rendering.core.FogSettings;
import it.hurks.rendering.core.Renderer;
import it.hurks.rendering.core.constants.VertexArrayIndices;
import it.hurks.rendering.core.entity.light.Light;
import it.hurks.rendering.core.terrain.Terrain;
import it.hurks.util.MathUtil;
import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL13;
import org.lwjgl.opengl.GL20;
import org.lwjgl.opengl.GL30;
import org.lwjgl.util.vector.Vector3f;

import java.util.List;

/**
 * This is the renderer class for the terrain.
 *
 * @author Hurks IT
 */
public class TerrainRenderer {

    private TerrainShader terrainShader;

    public TerrainShader getTerrainShader() {
        return terrainShader;
    }

    public TerrainRenderer(TerrainShader terrainShader) {
        this.terrainShader = terrainShader;
    }

    /**
     * Render all terrains
     *
     * @param terrains the terrains
     */
    public void renderTerrains (List<Terrain> terrains, List<Light> lights) {
        for (Terrain terrain : terrains) {
            GL30.glBindVertexArray(terrain.getRawModel().getVaoID());
            GL20.glEnableVertexAttribArray(VertexArrayIndices.POSITIONS);
            GL20.glEnableVertexAttribArray(VertexArrayIndices.TEXTURE_COORDINATES);
            GL20.glEnableVertexAttribArray(VertexArrayIndices.NORMALS);

            GL13.glActiveTexture(GL13.GL_TEXTURE0);
            GL11.glBindTexture(GL11.GL_TEXTURE_2D, terrain.getModelTexture().getTextureID());

            terrainShader.loadProjectionMatrix(Renderer.getProjectionMatrix());
            terrainShader.loadSpecularLightningValues(terrain.getModelTexture().getShineDamper(), terrain.getModelTexture().getReflectivity());
            for (Light light : lights) {
                terrainShader.loadLight(light);
            }
            terrainShader.loadFogSettings(FogSettings.DENSITY, FogSettings.GRADIENT, FogSettings.SKY_COLOR);
            terrainShader.loadTransformationMatrix(MathUtil.createTransformationMatrix(new Vector3f(terrain.getX(), 0, terrain.getZ()),
                    new Vector3f(0, 0, 0), new Vector3f(1, 1, 1)));
            GL11.glDrawElements(GL11.GL_TRIANGLES, terrain.getRawModel().getVertexCount(), GL11.GL_UNSIGNED_INT, 0);

            GL20.glDisableVertexAttribArray(VertexArrayIndices.POSITIONS);
            GL20.glDisableVertexAttribArray(VertexArrayIndices.TEXTURE_COORDINATES);
            GL20.glDisableVertexAttribArray(VertexArrayIndices.NORMALS);
            GL30.glBindVertexArray(0);
        }
    }
}
