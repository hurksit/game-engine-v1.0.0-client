package it.hurks.rendering.entity;

import it.hurks.rendering.core.constants.VertexArrayIndices;
import it.hurks.rendering.core.entity.light.Light;
import it.hurks.rendering.core.Shader;
import org.lwjgl.util.vector.Matrix4f;
import org.lwjgl.util.vector.Vector3f;

/**
 * The entity shader, this will be used for entity objects.
 *
 * @author Hurks IT
 */
public class EntityShader extends Shader {

    private static final String VERTEX_SHADER_LOCATION = "src/main/resources/shader/entity/entityVertexShader.glsl";

    private static final String FRAGMENT_SHADER_LOCATION = "src/main/resources/shader/entity/entityFragmentShader.glsl";

    private int locationTransformationMatrix;

    private int locationProjectionMatrix;

    private int locationViewMatrix;

    private int locationLightPosition;

    private int locationLightColor;

    private int locationShineDamper;

    private int locationReflectivity;

    private int locationUseFakeLightning;

    private int locationFogDensity;

    private int locationFogGradient;

    private int locationSkyColor;

    public EntityShader() {
        super(VERTEX_SHADER_LOCATION, FRAGMENT_SHADER_LOCATION);
    }

    /**
     * Loads the transformation matrix to GLSL.
     *
     * @param matrix the transformation matrix
     */
    public void loadTransformationMatrix (Matrix4f matrix) {
        super.loadMatrix(locationTransformationMatrix, matrix);
    }

    /**
     * Loads the projection matrix to GLSL.
     *
     * @param matrix the projection matrix
     */
    public void loadProjectionMatrix (Matrix4f matrix) {
        super.loadMatrix(locationProjectionMatrix, matrix);
    }

    /**
     * Loads the view matrix to GLSL.
     *
     * @param matrix the view matrix
     */
    public void loadViewMatrix (Matrix4f matrix) {
        super.loadMatrix(locationViewMatrix, matrix);
    }

    /**
     * Loads a light to GLSL.
     *
     * @param light the light to load.
     */
    public void loadLight (Light light) {
        super.loadVector3(locationLightPosition, light.getPosition());
        super.loadVector3(locationLightColor, light.getColor());
    }

    /**
     * Load the variables for specular lightning to OpenGL.
     *
     * @param shineDamper the distance the camera has to be in order to see any reflected light.
     * @param reflectivity the strength of the lights' reflection.
     * @param useFakeLightning whether or not to use fake lightning.
     */
    public void loadSpecularLightningValues (float shineDamper, float reflectivity, boolean useFakeLightning) {
        super.loadFloat(locationShineDamper, shineDamper);
        super.loadFloat(locationReflectivity, reflectivity);
        super.loadBoolean(locationUseFakeLightning, useFakeLightning);
    }

    /**
     * Load the fog settings to OpenGL.
     *
     * @param fogDensity the density
     * @param fogGradient the gradient
     * @param skyColor color of the sky
     */
    public void loadFogSettings (float fogDensity, float fogGradient, Vector3f skyColor) {
        super.loadFloat(locationFogDensity, fogDensity);
        super.loadFloat(locationFogGradient, fogGradient);
        super.loadVector3(locationSkyColor, skyColor);
    }

    @Override
    protected void getUniformLocations() {
        locationTransformationMatrix = getUniformLocation("transformationMatrix");
        locationProjectionMatrix = getUniformLocation("projectionMatrix");
        locationViewMatrix = getUniformLocation("viewMatrix");
        locationLightPosition = getUniformLocation("lightPosition");
        locationLightColor = getUniformLocation("lightColor");
        locationShineDamper = getUniformLocation("shineDamper");
        locationReflectivity = getUniformLocation("reflectivity");
        locationUseFakeLightning = getUniformLocation("useFakeLightning");
        locationFogDensity = getUniformLocation("fogDensity");
        locationFogGradient = getUniformLocation("fogGradient");
        locationSkyColor = getUniformLocation("skyColor");
    }

    @Override
    protected void bindAttributes() {
        super.bindAttribute(VertexArrayIndices.POSITIONS, "position");
        super.bindAttribute(VertexArrayIndices.TEXTURE_COORDINATES, "textureCoordinates");
        super.bindAttribute(VertexArrayIndices.NORMALS, "normals");
    }
}
