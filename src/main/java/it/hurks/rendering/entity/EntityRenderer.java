package it.hurks.rendering.entity;

import it.hurks.rendering.core.FogSettings;
import it.hurks.rendering.core.Renderer;
import it.hurks.rendering.core.constants.VertexArrayIndices;
import it.hurks.rendering.core.entity.Entity;
import it.hurks.rendering.core.entity.Model;
import it.hurks.rendering.core.entity.light.Light;
import it.hurks.rendering.core.entity.model.TexturedModel;
import it.hurks.util.MathUtil;
import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL13;
import org.lwjgl.opengl.GL20;
import org.lwjgl.opengl.GL30;

import java.util.List;
import java.util.Map;

/**
 * This class contains all entity rendering
 * related functionality.
 *
 * @author Hurks IT
 */
public class EntityRenderer {

    private EntityShader entityShader;

    public EntityRenderer(EntityShader entityShader) {
        this.entityShader = entityShader;
    }

    /**
     * Render an entity to the screen.
     * @param entities the entities.
     * @param lights the lights affecting this entity
     */
    public void renderEntities(Map<Model, List<Entity>> entities, List<Light> lights) {

        for (Model model : entities.keySet()) {
            if (model instanceof TexturedModel) {
                renderEntitiesForTexturedModel (entities.get(model), lights, (TexturedModel)model);
            }
        }
    }

    /**
     * Render all entities with the same textured model.
     *
     * @param entities the entities to render
     * @param lights the lights to render
     * @param texturedModel the textured model to use
     */
    private void renderEntitiesForTexturedModel (List<Entity> entities, List<Light> lights, TexturedModel texturedModel) {
        GL30.glBindVertexArray(texturedModel.getRawModel().getVaoID());
        GL20.glEnableVertexAttribArray(VertexArrayIndices.POSITIONS);
        GL20.glEnableVertexAttribArray(VertexArrayIndices.TEXTURE_COORDINATES);
        GL20.glEnableVertexAttribArray(VertexArrayIndices.NORMALS);

        GL13.glActiveTexture(GL13.GL_TEXTURE0);
        GL11.glBindTexture(GL11.GL_TEXTURE_2D, texturedModel.getTexture().getTextureID());

        if (texturedModel.getTexture().isTransparent()) {
            Renderer.disableCulling();
        } else {
            Renderer.enableCulling();
        }

        entityShader.loadProjectionMatrix(Renderer.getProjectionMatrix());
        entityShader.loadSpecularLightningValues(texturedModel.getTexture().getShineDamper(),
                texturedModel.getTexture().getReflectivity(), texturedModel.getTexture().isUseFakeLightning());
        for (Light light : lights) {
            entityShader.loadLight(light);
        }
        entityShader.loadFogSettings(FogSettings.DENSITY, FogSettings.GRADIENT, FogSettings.SKY_COLOR);

        for (Entity entity : entities) {
            entityShader.loadTransformationMatrix(MathUtil.createTransformationMatrix(entity.getTransformation().getPosition(),
                    entity.getTransformation().getRotation(), entity.getTransformation().getScale()));
            GL11.glDrawElements(GL11.GL_TRIANGLES, texturedModel.getRawModel().getVertexCount(), GL11.GL_UNSIGNED_INT, 0);
        }

        GL20.glDisableVertexAttribArray(VertexArrayIndices.POSITIONS);
        GL20.glDisableVertexAttribArray(VertexArrayIndices.TEXTURE_COORDINATES);
        GL20.glDisableVertexAttribArray(VertexArrayIndices.NORMALS);
        GL30.glBindVertexArray(0);
    }

}
