package it.hurks.rendering.core.texture;

/**
 * This class represents a texture to use
 * with models.
 *
 * @author Hurks IT
 */
public class ModelTexture {

    private int textureID;

    /**
     * How far the camera must be away from the
     * reflected light in order to see any difference
     * in brightness from the reflected
     * light.
     */
    private float shineDamper = 0;

    /**
     * The strength of the lights' reflection.
     */
    private float reflectivity = 1;

    private boolean isTransparent = false;

    private boolean useFakeLightning = false;

    public int getTextureID() {
        return textureID;
    }

    public float getShineDamper() {
        return shineDamper;
    }

    public void setShineDamper(float shineDamper) {
        this.shineDamper = shineDamper;
    }

    public float getReflectivity() {
        return reflectivity;
    }

    public void setReflectivity(float reflectivity) {
        this.reflectivity = reflectivity;
    }

    public boolean isTransparent() {
        return isTransparent;
    }

    public void setTransparent(boolean transparent) {
        isTransparent = transparent;
    }

    public boolean isUseFakeLightning() {
        return useFakeLightning;
    }

    public void setUseFakeLightning(boolean useFakeLightning) {
        this.useFakeLightning = useFakeLightning;
    }

    public ModelTexture(int textureID) {
        this.textureID = textureID;
    }

}
