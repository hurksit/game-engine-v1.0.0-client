package it.hurks.rendering.core;

import org.lwjgl.util.vector.Vector3f;

/**
 * This class contains all fog related functionality.
 *
 * @author Hurks IT
 */
public class FogSettings {

    /**
     * The thickness of the fog.
     */
    public static final float DENSITY = 0.003f;

    /**
     * The gradient of the fog.
     */
    public static final float GRADIENT = 1.25f;

    /**
     * The color of the sky.
     */
    public static final Vector3f SKY_COLOR = new Vector3f(0.2f, 0.5f, 0.8f);

}
