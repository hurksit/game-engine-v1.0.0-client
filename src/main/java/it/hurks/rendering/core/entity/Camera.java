package it.hurks.rendering.core.entity;

import org.lwjgl.input.Keyboard;
import org.lwjgl.util.vector.Vector3f;

/**
 * This class represents the in-game camera.
 *
 * @author Hurks IT
 */
public class Camera {

    private Vector3f position = new Vector3f(0, 5, 0);

    private float pitch = 15;

    private float yaw = 180;

    private float roll;

    public Vector3f getPosition() {
        return position;
    }

    public float getPitch() {
        return pitch;
    }

    public float getYaw() {
        return yaw;
    }

    public float getRoll() {
        return roll;
    }

    public Camera() {
    }

    /**
     * Move the camera
     */
    public void move () {
        float movementSpeed = 1f;
        if (Keyboard.isKeyDown(Keyboard.KEY_S)) {
            position.z += movementSpeed;
        }
        if (Keyboard.isKeyDown(Keyboard.KEY_W)) {
            position.z -= movementSpeed;
        }
        if (Keyboard.isKeyDown(Keyboard.KEY_D)) {
            yaw -= 1;
        }
        if (Keyboard.isKeyDown(Keyboard.KEY_A)) {
            yaw += 1;
        }
    }
}
