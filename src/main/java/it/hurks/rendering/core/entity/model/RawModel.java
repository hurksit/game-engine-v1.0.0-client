package it.hurks.rendering.core.entity.model;

/**
 * This class represents a 3D-model.
 *
 * @author Hurks IT
 */
public class RawModel {

    /**
     * The ID of the vertex attribute object
     */
    private int vaoID;

    /**
     * The amount of vertices within the model
     */
    private int vertexCount;

    public int getVaoID() {
        return vaoID;
    }

    public int getVertexCount() {
        return vertexCount;
    }

    public RawModel(int vaoID, int vertexCount) {
        this.vaoID = vaoID;
        this.vertexCount = vertexCount;
    }

}
