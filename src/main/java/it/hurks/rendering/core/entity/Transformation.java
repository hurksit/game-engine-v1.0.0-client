package it.hurks.rendering.core.entity;

import org.lwjgl.util.vector.Vector3f;

/**
 * This class represents the position, rotation and scale
 * of an object.
 *
 * @author Hurks IT
 */
public class Transformation {

    private Vector3f position;

    private Vector3f rotation;

    private Vector3f scale;

    public Vector3f getPosition() {
        return position;
    }

    public Vector3f getRotation() {
        return rotation;
    }

    public Vector3f getScale() {
        return scale;
    }

    public Transformation(Vector3f position, Vector3f rotation, Vector3f scale) {
        this.position = position;
        this.rotation = rotation;
        this.scale = scale;
    }
}
