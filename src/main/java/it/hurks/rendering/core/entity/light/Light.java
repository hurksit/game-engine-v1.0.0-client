package it.hurks.rendering.core.entity.light;

import org.lwjgl.util.vector.Vector3f;

/**
 * This class represents a light source in the game.
 *
 * @author Hurks IT
 */
public class Light {

    private Vector3f position;

    private Vector3f color;

    public Vector3f getPosition() {
        return position;
    }

    public Vector3f getColor() {
        return color;
    }

    public Light(Vector3f position, Vector3f color) {
        this.position = position;
        this.color = color;
    }
}
