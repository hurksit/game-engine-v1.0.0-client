package it.hurks.rendering.core.entity.model;

import it.hurks.rendering.core.entity.Model;
import it.hurks.rendering.core.texture.ModelTexture;

/**
 * This class represents a rawModel with a texture attached to it.
 *
 * @author Hurks IT
 */
public class TexturedModel extends Model {

    /**
     * The raw rawModel used for this rawModel.
     */
    private RawModel rawModel;

    /**
     * The texture used for this rawModel.
     */
    private ModelTexture texture;

    public RawModel getRawModel() {
        return rawModel;
    }

    public ModelTexture getTexture() {
        return texture;
    }

    public TexturedModel(RawModel rawModel, ModelTexture texture) {
        this.rawModel = rawModel;
        this.texture = texture;
    }

}
