package it.hurks.rendering.core.entity;

import org.lwjgl.util.vector.Vector3f;

/**
 * An entity in the game, having a model and a transformation.
 *
 * @author Hurks IT
 */
public class Entity {

    private Model model;

    private Transformation transformation;

    public Model getModel() {
        return model;
    }

    public Transformation getTransformation() {
        return transformation;
    }

    public Entity(Model model, Transformation transformation) {
        this.model = model;
        this.transformation = transformation;
    }

    /**
     * Increase the entities' position.
     *
     * @param delta the delta
     */
    public void increasePosition (Vector3f delta) {
        transformation.getPosition().x += delta.x;
        transformation.getPosition().y += delta.y;
        transformation.getPosition().z += delta.z;
    }

    /**
     * Increase the entities' rotation.
     *
     * @param delta the delta
     */
    public void increaseRotation (Vector3f delta) {
        transformation.getRotation().x += delta.x;
        transformation.getRotation().y += delta.y;
        transformation.getRotation().z += delta.z;
    }


    /**
     * Increase the entities' scale.
     *
     * @param delta the delta
     */
    public void increaseScale (Vector3f delta) {
        transformation.getScale().x += delta.x;
        transformation.getScale().y += delta.y;
        transformation.getScale().z += delta.z;
    }
}
