package it.hurks.rendering.core.constants;

/**
 * This class keeps track of which vertex attribute indices are
 * used to store which data within a VAO.
 *
 * @author Hurks IT
 */
public class VertexArrayIndices {

    /**
     * Vertex positions
     */
    public static final int POSITIONS = 0;

    /**
     * All texture coordinates
     */
    public static final int TEXTURE_COORDINATES = 1;

    /**
     * All normal positions
     */
    public static final int NORMALS = 2;

}
