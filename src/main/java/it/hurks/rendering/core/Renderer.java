package it.hurks.rendering.core;

import org.lwjgl.opengl.Display;
import org.lwjgl.opengl.GL11;
import org.lwjgl.util.vector.Matrix4f;

/**
 * This class handles all generic rendering
 * functionality.
 *
 * @author Hurks IT
 */
public class Renderer {

    private static final float FIELD_OF_VIEW = 70;

    private static final float NEAR_PLANE = 0.1f;

    private static final float FAR_PLANE = 1000f;

    private static Matrix4f projectionMatrix;

    private static boolean cullingEnabled = false;

    public static Matrix4f getProjectionMatrix() {
        return projectionMatrix;
    }

    /**
     * Update the viewport of the display holding the OpenGL context.
     *
     * @param width the width of the screen
     * @param height the height of the screen
     */
    public static void updateViewport (int width, int height) {
        GL11.glViewport(0, 0, width, height);
    }

    /**
     * Enable culling
     */
    public static void enableCulling () {
        if (!cullingEnabled) {
            GL11.glEnable(GL11.GL_CULL_FACE);
            GL11.glCullFace(GL11.GL_BACK);
            cullingEnabled = true;
        }
    }

    /**
     * Disable culling
     */
    public static void disableCulling () {
        if (cullingEnabled) {
            GL11.glDisable(GL11.GL_CULL_FACE);
            cullingEnabled = false;
        }
    }

    /**
     * Create a new projection matrix
     * and put it in the projectionMatrix properties' value.
     */
    public static void createProjectionMatrix () {
        float aspectRatio = (float) Display.getWidth() / (float) Display.getHeight ();
        float yScale = (float) (1f / Math.tan (Math.toRadians(FIELD_OF_VIEW / 2f)) * aspectRatio);
        float xScale = yScale / aspectRatio;
        float frustumLength = FAR_PLANE - NEAR_PLANE;

        projectionMatrix = new Matrix4f();
        projectionMatrix.m00 = xScale;
        projectionMatrix.m11 = yScale;
        projectionMatrix.m22 = -((FAR_PLANE + NEAR_PLANE) / frustumLength);
        projectionMatrix.m23 = -1;
        projectionMatrix.m32 = -((2 * NEAR_PLANE * FAR_PLANE) / frustumLength);
        projectionMatrix.m33 = 0;
    }

    /**
     * Prepare the rendering
     */
    public void prepare () {
        GL11.glEnable(GL11.GL_DEPTH_TEST);
        GL11.glClear(GL11.GL_COLOR_BUFFER_BIT|GL11.GL_DEPTH_BUFFER_BIT);
        GL11.glClearColor(FogSettings.SKY_COLOR.getX() , FogSettings.SKY_COLOR.getY(), FogSettings.SKY_COLOR.getZ(), 1);
    }

}
