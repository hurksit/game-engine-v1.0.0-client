package it.hurks.rendering.core;

import it.hurks.util.FileUtil;
import org.lwjgl.BufferUtils;
import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL20;
import org.lwjgl.util.vector.Matrix4f;
import org.lwjgl.util.vector.Vector3f;

import java.nio.FloatBuffer;

/**
 * This class represents a shader
 *
 * @author Hurks IT
 */
public abstract class Shader {

    private static FloatBuffer matrixBuffer = BufferUtils.createFloatBuffer(16);

    private int shaderProgramID;

    private int vertexShaderID;

    private int fragmentShaderID;

    /**
     * Constructs a new Shader object
     * @param vertexShaderLocation the location of the vertex shader file
     * @param fragmentShaderLocation the location of the fragment shader file
     */
    public Shader (String vertexShaderLocation, String fragmentShaderLocation) {
        shaderProgramID = GL20.glCreateProgram();

        vertexShaderID = loadShader(vertexShaderLocation, GL20.GL_VERTEX_SHADER);
        GL20.glAttachShader(shaderProgramID, vertexShaderID);

        fragmentShaderID = loadShader(fragmentShaderLocation, GL20.GL_FRAGMENT_SHADER);
        GL20.glAttachShader(shaderProgramID, fragmentShaderID);

        bindAttributes();
        GL20.glLinkProgram(shaderProgramID);
        GL20.glValidateProgram(shaderProgramID);

        getUniformLocations();
    }

    /**
     * Start the shader program
     */
    public void start () {
        GL20.glUseProgram(shaderProgramID);
    }

    /**
     * Stops the shader program
     */
    public void stop () {
        GL20.glUseProgram(0);
    }

    /**
     * Cleans up the shader program
     */
    public void cleanUp () {
        stop ();

        GL20.glDetachShader (shaderProgramID, vertexShaderID);
        GL20.glDeleteShader(vertexShaderID);

        GL20.glDetachShader (shaderProgramID, fragmentShaderID);
        GL20.glDeleteShader(fragmentShaderID);

        GL20.glDeleteProgram(shaderProgramID);
    }

    /**
     * Configure all uniform locations for GLSL.
     */
    protected abstract void getUniformLocations ();

    /**
     * Creates a uniform location ID for a uniform variable name.
     *
     * @param uniformName the uniform variable name
     * @return the uniform location id
     */
    protected int getUniformLocation (String uniformName) {
        return GL20.glGetUniformLocation(shaderProgramID, uniformName);
    }

    /**
     * Load a float to a uniform variable.
     *
     * @param uniformLocation the location of the uniform variable.
     * @param value the value
     */
    protected void loadFloat (int uniformLocation, float value) {
        GL20.glUniform1f(uniformLocation, value);
    }

    /**
     * Load an integer to a uniform variable.
     *
     * @param uniformLocation the location of the uniform variable.
     * @param value the value
     */
    protected void loadInt (int uniformLocation, int value) {
        GL20.glUniform1i(uniformLocation, value);
    }

    /**
     * Load a boolean to a uniform variable.
     *
     * @param uniformLocation the location of the uniform variable.
     * @param value the value
     */
    protected void loadBoolean (int uniformLocation, boolean value) {
        loadFloat (uniformLocation, value ? 1f : 0f);
    }

    /**
     * Load a matrix to a uniform variable.
     *
     * @param uniformLocation the location of the uniform variable.
     * @param matrix the matrix
     */
    protected void loadMatrix (int uniformLocation, Matrix4f matrix) {
        matrix.store(matrixBuffer);
        matrixBuffer.flip();
        GL20.glUniformMatrix4(uniformLocation, false, matrixBuffer);
    }

    /**
     * Load a vector3 to a uniform variable.
     *
     * @param uniformLocation the location of the uniform variable.
     * @param value the value
     */
    protected void loadVector3 (int uniformLocation, Vector3f value) {
        GL20.glUniform3f(uniformLocation, value.x, value.y, value.z);
    }

    /**
     * Binds the VAO attributes to the shader program.
     */
    protected abstract void bindAttributes ();

    /**
     * Bind a VAO attribute to the shader programs' input variable.
     * @param attributeLocation the attribute location
     * @param variableName the name of the variable in the shader program
     */
    protected void bindAttribute (int attributeLocation, String variableName) {
        GL20.glBindAttribLocation(shaderProgramID, attributeLocation, variableName);
    }

    /**
     * Loads a shader from a file, compiles it and binds it to the OpenGL context.
     *
     * @param shaderFile the location of the shader file
     * @param shaderType the process of shader (GL20.GL_VERTEX_SHADER || GL20.GL_FRAGMENT_SHADER)
     * @return the shader ID
     */
    private static int loadShader (String shaderFile, int shaderType) {
        int shaderID = GL20.glCreateShader(shaderType);
        GL20.glShaderSource(shaderID, FileUtil.getContents(shaderFile));
        GL20.glCompileShader(shaderID);

        if (GL20.glGetShader(shaderID, GL20.GL_COMPILE_STATUS) == GL11.GL_FALSE) {
            System.out.println(GL20.glGetShaderInfoLog(shaderID, 500));
            System.err.println("Could not compile shader.");
            System.exit(-1);
        }
        return shaderID;
    }

}
