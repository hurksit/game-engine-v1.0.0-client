package it.hurks.rendering.core.loader;

import it.hurks.rendering.core.entity.model.RawModel;
import org.lwjgl.util.vector.Vector3f;

import javax.vecmath.Vector2f;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * This class contains all functionality related
 * to loading .obj files.
 *
 * @author Hurks IT
 */
public class OBJLoader {

    /**
     * Load an obj model
     *
     * @param fileName the file name to the .obj file
     * @param loader the loader
     * @return the raw model
     */
    public static RawModel loadObjModel (String fileName, Loader loader) {
        FileReader fileReader = null;
        try {
            fileReader = new FileReader(new File(fileName));
        } catch (FileNotFoundException e) {
            System.err.println("Could not find .obj file.");
            e.printStackTrace();
        }

        BufferedReader reader = new BufferedReader(fileReader);
        List<Vector3f> vertices = new ArrayList<>();
        List<Vector2f> textureCoordinates = new ArrayList<>();
        List<Vector3f> normalVertices = new ArrayList<>();
        List<Integer> vertexIndices = new ArrayList<>();
        float[] verticesArray = null;
        float[] normalVerticesArray = null;
        float[] textureCoordinatesArray = null;
        int[] vertexIndicesArray = null;
        String line;

        try {
            while (true) {
                line = reader.readLine();
                String[] currentLine = line.split(" ");
                if (line.startsWith("v ")) {
                    Vector3f vertex = new Vector3f(Float.parseFloat(currentLine[1]),
                            Float.parseFloat(currentLine[2]), Float.parseFloat(currentLine[3]));
                    vertices.add(vertex);
                }
                else if (line.startsWith("vt ")) {
                    Vector2f texture = new Vector2f(Float.parseFloat(currentLine[1]), Float.parseFloat(currentLine[2]));
                    textureCoordinates.add(texture);
                }
                else if (line.startsWith("vn ")) {
                    Vector3f normal = new Vector3f(Float.parseFloat(currentLine[1]), Float.parseFloat(currentLine[2]),
                            Float.parseFloat(currentLine[3]));
                    normalVertices.add(normal);
                }
                else if (line.startsWith("f ")) {
                    textureCoordinatesArray = new float[vertices.size()*2];
                    normalVerticesArray = new float[vertices.size()*3];
                    break;
                }
            }

            while (line != null) {
                if (!line.startsWith("f ")) {
                    line = reader.readLine();
                    continue;
                }

                String[] currentLine = line.split(" ");
                for (int i = 0; i < 3; i ++) {
                    processVertex(currentLine[i + 1].split("/"), vertexIndices, textureCoordinates, normalVertices,
                            textureCoordinatesArray, normalVerticesArray);
                }
                line = reader.readLine();
            }
            reader.close();

            verticesArray = new float[vertices.size() * 3];
            vertexIndicesArray = new int[vertexIndices.size()];

            int vertexPointer = 0;
            for (Vector3f vertex : vertices) {
                verticesArray[vertexPointer++] = vertex.x;
                verticesArray[vertexPointer++] = vertex.y;
                verticesArray[vertexPointer++] = vertex.z;
            }

            for (int i = 0; i < vertexIndices.size(); i ++) {
                vertexIndicesArray[i] = vertexIndices.get(i);
            }
        } catch (IOException e) {
            System.err.println("Could not process .obj file.");
            e.printStackTrace();
        }

        return loader.loadToVAO(verticesArray, textureCoordinatesArray, normalVerticesArray, vertexIndicesArray);
    }

    /**
     * Process a vertex
     *
     * @param vertexData the split string vertex data
     * @param vertexIndices the list of vertex indices
     * @param textureCoordinates the list of texture coordinates
     * @param normalVertices the list of normal indices
     * @param textureCoordinatesArray the array of texture coordinates
     * @param normalVerticesArray the array of normal vertices
     */
    private static void processVertex (String[] vertexData, List<Integer> vertexIndices, List<Vector2f> textureCoordinates,
                                       List<Vector3f> normalVertices, float[] textureCoordinatesArray, float[] normalVerticesArray) {
        int currentVertexPointer = Integer.parseInt(vertexData[0]) - 1;
        vertexIndices.add(currentVertexPointer);
        Vector2f currentTexture = textureCoordinates.get(Integer.parseInt(vertexData[1])-1);
        textureCoordinatesArray[currentVertexPointer*2] = currentTexture.x;
        textureCoordinatesArray[currentVertexPointer*2 + 1] = 1 - currentTexture.y;
        Vector3f currentNormalVertex = normalVertices.get(Integer.parseInt(vertexData[2]) - 1);
        normalVerticesArray[currentVertexPointer * 3] = currentNormalVertex.x;
        normalVerticesArray[currentVertexPointer * 3 + 1] = currentNormalVertex.y;
        normalVerticesArray[currentVertexPointer * 3 + 2] = currentNormalVertex.z;
    }

}
