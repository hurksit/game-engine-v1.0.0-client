package it.hurks.rendering.core.loader;

import it.hurks.rendering.core.entity.model.RawModel;
import it.hurks.rendering.core.constants.VertexArrayIndices;
import org.lwjgl.BufferUtils;
import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL15;
import org.lwjgl.opengl.GL20;
import org.lwjgl.opengl.GL30;
import org.newdawn.slick.opengl.Texture;
import org.newdawn.slick.opengl.TextureLoader;

import java.io.FileInputStream;
import java.io.IOException;
import java.nio.FloatBuffer;
import java.nio.IntBuffer;
import java.util.ArrayList;
import java.util.List;

/**
 * This manager class will contain all functionality
 * related to loading things to the OpenGL context.
 *
 * @author Hurks IT
 */
public class Loader {

    private List<Integer> vaoIDs = new ArrayList<>();

    private List<Integer> vboIDs = new ArrayList<>();

    private List<Integer> textureIDs = new ArrayList<>();

    /**
     * Loads a list of vertex positions to a VAO.
     *
     * This method will return a RawModel object with the
     * ID of the created VAO.
     *
     * @param vertexPositions the array of vertex positions.
     * @param textureCoordinates the array of texture coordinates
     * @param normals the array of normal indices
     * @param vertexIndices the array of vertex indices
     *
     * @return the model object
     */
    public RawModel loadToVAO (float[] vertexPositions, float[] textureCoordinates, float[] normals, int[] vertexIndices) {
        int vaoID = createVAO();
        bindVertexIndices(vertexIndices);
        storeDataInVertexArray(VertexArrayIndices.POSITIONS, 3, vertexPositions);
        storeDataInVertexArray(VertexArrayIndices.TEXTURE_COORDINATES, 2, textureCoordinates);
        storeDataInVertexArray(VertexArrayIndices.NORMALS, 3, normals);
        unbindVAO();

        return new RawModel(vaoID, vertexIndices.length);
    }

    /**
     * Load a texture to the OpenGL context.
     *
     * @param fileName the file location of the texture.
     * @return the id of the texture
     */
    public int loadTexture (String fileName) {
        Texture texture = null;
        try {
            texture = TextureLoader.getTexture("png", new FileInputStream(fileName));
        } catch (IOException e) {
            System.err.println("Could not find texture: " + fileName);
            e.printStackTrace();
        }
        int textureID = texture.getTextureID();
        textureIDs.add(textureID);

        return textureID;
    }

    /**
     * Cleans up all resources.
     */
    public void cleanUp () {
        vaoIDs.forEach(GL30::glDeleteVertexArrays);
        vboIDs.forEach(GL15::glDeleteBuffers);
        textureIDs.forEach(GL11::glDeleteTextures);
    }

    /**
     * Stores an array of data in one of the selected
     * Vertex Array Objects' vertex arrays.
     *
     * @param arrayIndex the index of the vertex array
     * @param coordinateSize the size of the coordinate
     * @param data the data to store
     */
    private void storeDataInVertexArray (int arrayIndex, int coordinateSize, float[] data) {
        createVBO();
        GL15.glBufferData(GL15.GL_ARRAY_BUFFER, convertFloatArrayToFloatBuffer(data), GL15.GL_STATIC_DRAW);
        GL20.glVertexAttribPointer(arrayIndex, coordinateSize, GL11.GL_FLOAT, false, 0, 0);
        unbindVBO();
    }

    /**
     * Binds the vertex indices to the selected Vertex Array Object.
     *
     * @param data the indices to bind
     */
    private void bindVertexIndices (int[] data) {
        int vboID = createVBO();
        GL15.glBindBuffer(GL15.GL_ELEMENT_ARRAY_BUFFER, vboID);
        GL15.glBufferData(GL15.GL_ELEMENT_ARRAY_BUFFER, convertIntArrayToIntBuffer(data), GL15.GL_STATIC_DRAW);
    }

    /**
     * Creates an int buffer from an int array
     *
     * @param data the int array
     * @return the int buffer
     */
    private IntBuffer convertIntArrayToIntBuffer (int[] data) {
        IntBuffer intBuffer = BufferUtils.createIntBuffer(data.length);
        intBuffer.put(data);
        intBuffer.flip();
        return intBuffer;
    }

    /**
     * Creates a float buffer from a float array.
     *
     * @param data the float array
     * @return the float buffer
     */
    private FloatBuffer convertFloatArrayToFloatBuffer(float[] data) {
        FloatBuffer floatBuffer = BufferUtils.createFloatBuffer(data.length);
        floatBuffer.put(data);
        floatBuffer.flip();
        return floatBuffer;
    }

    /**
     * Creates a new Vertex Array Object
     * @return the ID of the VAO
     */
    private int createVAO () {
        int vaoID = GL30.glGenVertexArrays();
        vaoIDs.add(vaoID);
        GL30.glBindVertexArray(vaoID);
        return vaoID;
    }

    /**
     * Creates a new Vertex Buffer Object
     * @return the ID
     */
    private int createVBO () {
        int vboID = GL15.glGenBuffers();
        vboIDs.add(vboID);
        GL15.glBindBuffer(GL15.GL_ARRAY_BUFFER, vboID);
        return vboID;
    }

    /**
     * Unbind a VAO after using.
     */
    private void unbindVAO() {
        GL30.glBindVertexArray(0);
    }

    /**
     * Unbind a Vertex Buffer Object
     */
    private void unbindVBO () {
        GL15.glBindBuffer(GL15.GL_ARRAY_BUFFER, 0);
    }
}
