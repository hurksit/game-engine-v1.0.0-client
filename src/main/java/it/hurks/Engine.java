package it.hurks;

import it.hurks.gameloop.GameLoop;
import it.hurks.gameloop.MainGameLoop;

/**
 * The main file where everything starts from.
 *
 * @author Hurks IT
 */
public final class Engine {

    /**
     * The main function used to launch the application.
     * @param args the arguments given
     */
    public static void main(String[] args) {

        GameLoop gameLoop = new MainGameLoop();

        gameLoop.initialize();

        gameLoop.run();

        gameLoop.cleanUp();

    }

}
