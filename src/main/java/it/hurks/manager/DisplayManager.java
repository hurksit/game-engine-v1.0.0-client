package it.hurks.manager;

import it.hurks.rendering.core.Renderer;
import it.hurks.rendering.entity.EntityRenderer;
import org.lwjgl.LWJGLException;
import org.lwjgl.opengl.*;

/**
 * This manager class will manage all
 * screen related functionality.
 *
 * @author Hurks IT
 */
public final class DisplayManager {

    private EntityRenderer entityRenderer;

    private static final int MAX_FPS = 150;

    private static long lastTime = 0;

    private static float fps = 0;

    private static int width;

    private static int height;

    private static String title;

    public static float getFps() {
        return fps;
    }

    public DisplayManager(EntityRenderer entityRenderer) {
        this.entityRenderer = entityRenderer;
    }

    /**
     * Initialize the display.
     * @param width width of the display
     * @param height height of the display
     * @param title title of the display
     */
    public void initialize (int width, int height, String title) {
        DisplayManager.width = width;
        DisplayManager.height = height;
        DisplayManager.title = title;

        ContextAttribs contextAttribs = new ContextAttribs(3,2)
                .withForwardCompatible(true)
                .withProfileCore(true);

        try {
            Display.setDisplayMode(new DisplayMode(width, height));
            Display.setFullscreen(false);
            Display.setVSyncEnabled(true);
            Display.setResizable(true);
            Display.setTitle(title);
            Display.create(new PixelFormat(), contextAttribs);
        } catch (LWJGLException e) {
            e.printStackTrace();
        }

        Renderer.updateViewport(width, height);
        Renderer.enableCulling();
        Renderer.createProjectionMatrix();
    }

    /**
     * Update the display
     */
    public void update () {
        Display.sync(MAX_FPS);

        fps = 1 / (Math.max(System.nanoTime() - lastTime, 1) * (float)Math.pow(10, -9));
        lastTime = System.nanoTime();

        Display.setTitle(title + " (FPS: " + fps + ")");

        if (Display.wasResized()) {
            Renderer.createProjectionMatrix();
            Renderer.updateViewport(Display.getWidth(), Display.getHeight());
        }

        Display.update();
    }

    /**
     * Clean up the display
     */
    public void cleanUp () {
        Display.destroy();
    }

}
