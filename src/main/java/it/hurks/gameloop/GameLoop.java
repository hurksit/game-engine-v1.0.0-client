package it.hurks.gameloop;

/**
 * This interface represents a game loop.
 *
 * @author Hurks IT
 */
public interface GameLoop {

    /**
     * Initialize everything
     * for the game loop
     */
    void initialize();

    /**
     * Run the game loop.
     */
    void run();

    /**
     * Clean up everything used
     * in the game loop.
     */
    void cleanUp();

}