package it.hurks.gameloop;

import it.hurks.manager.DisplayManager;
import it.hurks.rendering.core.Renderer;
import it.hurks.rendering.core.entity.Entity;
import it.hurks.rendering.core.entity.Model;
import it.hurks.rendering.core.entity.light.Light;
import it.hurks.rendering.core.entity.model.TexturedModel;
import it.hurks.rendering.core.entity.Transformation;
import it.hurks.rendering.core.loader.OBJLoader;
import it.hurks.rendering.core.entity.Camera;
import it.hurks.rendering.core.terrain.Terrain;
import it.hurks.rendering.entity.EntityShader;
import it.hurks.rendering.entity.EntityRenderer;
import it.hurks.rendering.core.loader.Loader;
import it.hurks.rendering.core.Shader;
import it.hurks.rendering.core.texture.ModelTexture;
import it.hurks.rendering.terrain.TerrainRenderer;
import it.hurks.rendering.terrain.TerrainShader;
import it.hurks.util.MathUtil;
import org.lwjgl.opengl.Display;
import org.lwjgl.util.vector.Vector3f;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

/**
 * The main game loop.
 *
 * @author Hurks IT
 */
public final class MainGameLoop implements GameLoop {

    private DisplayManager displayManager;
    private Renderer renderer = new Renderer();
    private Loader loader = new Loader();

    private EntityShader entityShader;
    private EntityRenderer entityRenderer;
    private Map<Model, List<Entity>> entities = new HashMap<>();
    private List<Light> lights = new ArrayList<>();

    private Camera camera = new Camera();
    private TerrainRenderer terrainRenderer;
    private TerrainShader terrainShader;
    private List<Terrain> terrains = new ArrayList<>();

    /**
     * @inheritDoc
     */
    public void initialize() {
        System.out.println("Initializing game loop.");

        displayManager = new DisplayManager(entityRenderer);
        displayManager.initialize(1440,810, "Game Engine v1.0.0");

        entityShader = new EntityShader();
        terrainShader = new TerrainShader();

        entityRenderer = new EntityRenderer(entityShader);
        terrainRenderer = new TerrainRenderer(terrainShader);

        lights.add(new Light(new Vector3f(0, 0, 20), new Vector3f(1, 1, 1)));

        terrains.add(new Terrain(0, 0, new ModelTexture(loader.loadTexture("src/main/resources/texture/ground/grass/grass1/grass1.png")), loader));
        terrains.add(new Terrain(1, 0, new ModelTexture(loader.loadTexture("src/main/resources/texture/ground/grass/grass1/grass1.png")), loader));

        addTemporaryEntities();
    }

    /**
     * @inheritDoc
     */
    public void run() {
        System.out.println("Game loop has started.");

        while (!Display.isCloseRequested()) {
            renderer.prepare();

            camera.move();

            entityShader.start();
            entityShader.loadViewMatrix(MathUtil.createViewMatrix(camera));
            entityRenderer.renderEntities(entities, lights);
            entityShader.stop();

            terrainShader.start();
            terrainShader.loadViewMatrix(MathUtil.createViewMatrix(camera));
            terrainRenderer.renderTerrains(terrains, lights);
            terrainShader.stop();

            displayManager.update();
        }
    }

    /**
     * @inheritDoc
     */
    public void cleanUp() {
        System.out.println("Cleaning up game loop.");

        loader.cleanUp();

        entityShader.cleanUp();
        terrainShader.cleanUp();

        displayManager.cleanUp();
    }

    /**
     * Adds the temporary entities.
     */
    private void addTemporaryEntities () {
        Model model = new TexturedModel(
                OBJLoader.loadObjModel("src/main/resources/object/stall/stall.obj", loader),

                new ModelTexture(loader.loadTexture("src/main/resources/object/stall/stall.png"))
        );
        List<Entity> entityList = new ArrayList<>();
        for (int i = 0; i < 3; i ++) {
            entityList.add(new Entity (
                    model,
                    new Transformation(
                            new Vector3f(20 - (i*20), -2.5f, -40),
                            new Vector3f(0, 0, 0),
                            new Vector3f(1,1,1)
                    )
            ));
        }
        entities.put(model, entityList);

        List<Entity> grasses = new ArrayList<>();
        TexturedModel grass = new TexturedModel(OBJLoader.loadObjModel("src/main/resources/object/quad/grass/grass1/grass1.obj", loader), new ModelTexture(loader.loadTexture("src/main/resources/object/quad/grass/grass1/grass1.png")));
        grass.getTexture().setUseFakeLightning(true);
        grass.getTexture().setTransparent(true);

        List<Entity> flowers = new ArrayList<>();
        TexturedModel flower = new TexturedModel(OBJLoader.loadObjModel("src/main/resources/object/quad/flower/flower1/flower1.obj", loader), new ModelTexture(loader.loadTexture("src/main/resources/object/quad/flower/flower1/flower1.png")));
        flower.getTexture().setUseFakeLightning(true);
        flower.getTexture().setTransparent(true);

        List<Entity> ferns = new ArrayList<>();
        TexturedModel fern = new TexturedModel(OBJLoader.loadObjModel("src/main/resources/object/quad/fern/fern1/fern1.obj", loader), new ModelTexture(loader.loadTexture("src/main/resources/object/quad/fern/fern1/fern1.png")));
        fern.getTexture().setUseFakeLightning(true);
        fern.getTexture().setTransparent(true);

        Random random = new Random();
        for (int i = 0; i < 1000; i ++) {
            int x = random.nextInt(1600),
                    z = random.nextInt(800);

            grasses.add(new Entity (
                    grass,

                    new Transformation(new Vector3f(x/3, 0, z/3), new Vector3f(0, 0, 0), new Vector3f(1, 1, 1))
            ));

            flowers.add(new Entity (
                    flower,

                    new Transformation(new Vector3f(x/2, 0, z/2), new Vector3f(0, 0, 0), new Vector3f(1, 1, 1))
            ));

            ferns.add(new Entity (
                    fern,

                    new Transformation(new Vector3f(x, 0, z), new Vector3f(0, 0, 0), new Vector3f(1, 1, 1))
            ));
        }
        entities.put(grass, grasses);
        entities.put(flower, flowers);
        entities.put(fern, ferns);
    }

}
