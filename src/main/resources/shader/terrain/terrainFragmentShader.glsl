#version 400 core

in vec2 passTextureCoordinates;

in vec3 surfaceNormalVector;
in vec3 directionToLightVector;
in vec3 directionToCameraVector;
in float visibility;

out vec4 outputColor;

uniform sampler2D textureSampler;
uniform vec3 lightColor;
uniform vec3 skyColor;
uniform float shineDamper;
uniform float reflectivity;

vec4 calculateTexture () {
    return texture (textureSampler, passTextureCoordinates);
}

float calculateBrightness () {
    vec3 unitSurficeNormalVector = normalize(surfaceNormalVector);
    vec3 unitDirectionToLightVector = normalize(directionToLightVector);
    return dot (unitSurficeNormalVector, unitDirectionToLightVector);
}

float calculateSpecularLightning () {
    vec3 unitSurficeNormalVector = normalize(surfaceNormalVector);
    vec3 unitDirectionToLightVector = normalize(directionToLightVector);
    vec3 unitDirectionToCameraVector = normalize(directionToCameraVector);
    vec3 lightDirection = -unitDirectionToLightVector;
    vec3 lightReflection = reflect (lightDirection, unitSurficeNormalVector);
    vec3 unitLightReflection = normalize(lightReflection);
    return pow(dot(unitLightReflection, unitDirectionToCameraVector), shineDamper) * reflectivity;
}

vec4 calculateLightning () {
    return max(vec4 (lightColor, 1.0) * calculateBrightness() * calculateSpecularLightning(), 0.3);
}

void main () {
    outputColor = calculateLightning() * calculateTexture();
    outputColor = mix(vec4 (skyColor, 1.0), outputColor, visibility);
}