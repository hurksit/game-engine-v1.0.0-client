#version 400 core

in vec3 position;
in vec2 textureCoordinates;
in vec3 normal;

out vec2 passTextureCoordinates;
out vec3 surfaceNormalVector;
out vec3 directionToLightVector;
out vec3 directionToCameraVector;
out float visibility;

uniform mat4 transformationMatrix;
uniform mat4 projectionMatrix;
uniform mat4 viewMatrix;
uniform vec3 lightPosition;

uniform float fogDensity;
uniform float fogGradient;

void processTexture () {
    passTextureCoordinates = textureCoordinates * 256;
}

void processLights (vec4 worldPosition) {
    surfaceNormalVector = (transformationMatrix * vec4 (normal, 0.0)).xyz;
    directionToLightVector = lightPosition - worldPosition.xyz;
}

void processVectorToCamera (vec4 worldPosition) {
    directionToCameraVector = (inverse(viewMatrix) * vec4(0, 0, 0, 1.0)).xyz - worldPosition.xyz;
}

void main() {
    vec4 worldPosition = transformationMatrix * vec4(position.xyz, 1.0);
    vec4 positionRelativeToCam = viewMatrix * worldPosition;
    float distanceToCam = length(positionRelativeToCam.xyz);
    visibility = clamp(exp(-pow((distanceToCam*fogDensity), fogGradient)), 0, 1);

    gl_Position = projectionMatrix * positionRelativeToCam;

    processTexture();
    processLights(worldPosition);
    processVectorToCamera(worldPosition);
}

